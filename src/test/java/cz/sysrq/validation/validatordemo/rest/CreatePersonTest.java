package cz.sysrq.validation.validatordemo.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.sysrq.validation.validatordemo.model.Dimensions;
import cz.sysrq.validation.validatordemo.model.PersonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class CreatePersonTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void createPerson() throws Exception {
        PersonDto req = PersonDto.builder()
                .name("Karel")
                .birthDate(LocalDate.of(1999, 5, 11))
                .dimensions(Dimensions.builder()
                        .weight(80)
                        .build())
                .build();

        ResultActions result = mockMvc.perform(put("/api/person")
                .headers(headers())
                .content(mapper.writeValueAsString(req)));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("name").value("Karel"))
                .andExpect(jsonPath("birthDate").value("1999-05-11"))
                .andExpect(jsonPath("dimensions.weight").value("80"))
                ;
    }

    private HttpHeaders headers() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }
}
