package cz.sysrq.validation.validatordemo.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class PersonDto {

    private String name;
    private LocalDate birthDate;
    private Dimensions dimensions;
}
