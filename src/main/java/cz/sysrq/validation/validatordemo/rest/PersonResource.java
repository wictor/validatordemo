package cz.sysrq.validation.validatordemo.rest;

import cz.sysrq.validation.validatordemo.model.PersonDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "api/person")
@Slf4j
public class PersonResource {

    private final Set<PersonDto> persons = new HashSet<>();

    @PutMapping
    public ResponseEntity<PersonDto> putPerson(@RequestBody PersonDto request) {
        log.info("PUT person {}", request);
        persons.add(request);
        return ResponseEntity.ok(request);
    }

    @GetMapping
    public ResponseEntity<List<PersonDto>> getPersons() {
        log.info("GET persons");
        return ResponseEntity.ok(List.copyOf(persons));
    }

}
